<?php
function getSQLConn($host, $u, $pwd, $db) {
    // sqlsay('host=' . $host . ',user=' . $u . ',pwd=' . $pwd . ',db=' . $db);
    $conn = new mysqli ( $host, $u, $pwd, $db );
    if ($conn->connect_errno) {
        sqlerr ( "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error );
        sys_err_stop ();
        return false;
    } else {
        sqlsay ( 'conn pass' );
        
        /**
         * Must to set charset to UTF-8 before output
         */
        $conn->query ( "SET NAMES 'utf8'" );
        $conn->query ( "SET CHARACTER_SET_CLIENT=utf8" );
        $conn->query ( "SET CHARACTER_SET_RESULTS=utf8" );
        
        return $conn;
    }
}

/**
 * template connecting to local MySQL in db 'users'
 */
function getSQLConnUser($u, $pwd) {
    return getSQLConn ( 'localhost', $u, $pwd, 'on_hand_media' );
}
?>