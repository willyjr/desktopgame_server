<?php
/*
 * Push it! send msg to one single target user via Parse.com push service
 */
include '../func/general.inc';
include '../par/parse.inc';
include '../par/mstr.inc'; // WY:Make sure you need this

$parse_push_url = "https://api.parse.com/1/installations";

/**
 * Compose POST msg to trigger push via curl
 */
$ch = curl_init ();

// Set URL and select POST
curl_setopt ( $ch, CURLOPT_URL, $parse_push_url );
curl_setopt ( $ch, CURLOPT_CUSTOMREQUEST, "GET" );

// HEADER
$post_header_app_id = "X-Parse-Application-Id: " . $parse_application_id;
$post_header_rst_k = "X-Parse-REST-API-Key: " . $parse_rest_api_key;
$post_header_mstr_k = "X-Parse-Master-Key: " . $parse_master_api_key;

$post_header_type = "Content-Type: application/json";

curl_setopt ( $ch, CURLOPT_HEADER, true );
curl_setopt ( $ch, CURLOPT_HTTPHEADER, array (
        $post_header_app_id,
        $post_header_mstr_k,
        $post_header_type 
) );

// DATA
$data_where = "";
$data_json_to_usr = "";
// TODO:in real life you should use something like:
// curl_setopt($ch, CURLOPT_POSTFIELDS,
// http_build_query(array('postvar1' => 'value1')));
$post_data = '{
        "where": {
            "$relatedTo":{
  				"object":{
  					"__type":"Pointer",
  					"className":"PublicInfo",
                    "objectId":"OuJTGwUFB3"
  				},
  				"key":"installations"
  			}
        },
        "data": {
          "alert": "Free hotdogs at the Parse concession stand!"' . mt_rand ( 0, 9 ) . '
        }
      }';

curl_setopt ( $ch, CURLOPT_POSTFIELDS, $post_data );
echo 'post data=' . $post_data;

// receive server response
curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );

// Send out the query
$server_output = curl_exec ( $ch );
$error = curl_errno ( $ch );

// CLOSE
curl_close ( $ch );

// further processing ....
say ( 'push_result:' . $server_output );
if ($error) {
    say ( '[Error] Parse push err:' . $error );
}
?>
