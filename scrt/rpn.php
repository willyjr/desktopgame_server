<?php
// Include
include './func/general.inc';
include './func/t.inc';
include './ccc/sql_acc.inc';
include './ccc/sql_conn.inc';
include './pri/init_acc_parse.php';

include './par/parse.inc';
include './par/mstr.inc'; // WY:Make sure need this

/**
 * Initial registration with phone number.
 * If user's already registered with other method, should call apn.php (add phone number) instead
 */

/**
 * Read from POST
 */
$json_string = $_POST ['user'];
$ui = json_decode ( $json_string, TRUE );

/**
 * Report error and stop here if check failed
 */
if (is_null ( $ui )) {
    sys_err_stop ();
}

/**
 * Extract user info
 */
$phone = $ui ["phone_number"];
$parse_inst_obj_id = $ui ["parse_inst_obj_id"];

/**
 * *************
 * ROUTINE START
 * *************
 */
function IsNullOrEmptyString($pPhone) {
    return (! isset ( $pPhone ) || trim ( $pPhone ) === '');
}
function get_sql_insert_user($pPhone) {
    
    $sql = "INSERT INTO `users` (`phone`)" . " VALUES ('" . $pPhone . "')";
    
    return $sql;
}
/**
 * *************
 * ROUTINE END
 * **************
 */

/**
 * Start Action here!
 */
// verify user data
if (IsNullOrEmptyString ( $phone )) {
    sys_err_stop ();
}

// setup SQL query
$sql_qry_if_phone_exists = "SELECT * FROM `users` WHERE `phone` = '" . $phone . "'";

// access SQL server
if (! ($conn = getSQLConnUser ( $sql_acc, $sql_pwd ))) {
    sqlsay ( 'SQL conn err' );
    sqlerr ( "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error );
    
    // TODO:Report error and stop here
    sys_err_stop ();
}

// query if existed user, insert if yes
if (($res = $conn->query ( $sql_qry_if_phone_exists ))) {
    
    /**
     * Ensure `user` table has the incoming user.
     * If new user, create row for it
     */
    if ($res->num_rows <= 0) {
        // Insert New User to db
        sqlsay ( 'new user' );
        $sql_insert_user = get_sql_insert_user ( $phone );
        sqlsay ( $sql_insert_user );
        $res_i = $conn->query ( $sql_insert_user );
    } else {
        // Existed User
        sqlsay ( 'existed user' );
    }
    
    /**
     * Refresh token
     */
    global $TGID_LEN;
    do {
        $t = getToken ();
    } while ( is_token_exist ( $conn, $t ) );
    
    $tgid = substr ( $t, 0, $TGID_LEN );
    
    // Get UID from `user` table to further identify in other tables
    $sql_get_uid = "SELECT `uid` FROM `users` WHERE `phone`='" . $phone . "'";
    sqlsay ( $sql_get_uid );
    
    if (($res = $conn->query ( $sql_get_uid ))) {
        $uid = $res->fetch_object ()->uid;
    } else {
        sys_err_stop ();
    }
    
    $sql_exist_t = "SELECT * FROM `tokens` WHERE `uid`= " . $uid;
    // sqlsay($sql_exist_t);
    
    if ($res = $conn->query ( $sql_exist_t )) {
        if ($res->num_rows > 0)         // existed token, update token
        {
            // update token
            $sql_update_t = "UPDATE `tokens` SET  `token` =  '" . $t . "', `tgid`='" . $tgid . "' WHERE  `uid` = " . $uid;
            
            /* free result set */
            $res->close ();
        } else         // token not found, insert to tokens table
        {
            // insert token
            $sql_update_t = "INSERT INTO `tokens` (
							`uid` ,
							`token` ,
							`updated_at` ,
							`tgid`
							)
							VALUES (
							'" . $uid . "',  '" . $t . "',
							" . time () . " ,  '" . $tgid . "'
							);";
        }
    }
    
    // Send UPDATE/INSERT command to SQL server
    $conn->query ( $sql_update_t );
    sqlsay ( $sql_update_t );
    
    /**
     * Grant public ID (pid)
     */
    // Check if pid existed and generate if none
    $sql_exist_pid = "SELECT * FROM `pid` WHERE `invalid` = 0 AND `uid`= " . $uid;
    sqlsay ( $sql_exist_pid );
    
    if ($res = $conn->query ( $sql_exist_pid )) {
        if ($res->num_rows > 0)         // Existed pid, reuse pid
        {
            $gPid = $res->fetch_object ()->pid;

        } else {
            // New User: Use Parse.com Installation Object ID as user's PID
            // NOTICE:Take care if not using Parse.com service
            $gPid = $parse_inst_obj_id;
            
            // Save pid to db
            $sql_create_pid = "INSERT INTO `pid` (
							`uid` ,
							`pid` ,
                            `play_reg_id`,
							`updated_at`
							)
							VALUES (
							'" . $uid . "',  '" . $gPid . "',  '" . $play_reg_id . "'," . time () . ");";
            sqlsay ( $sql_create_pid );
            $conn->query ( $sql_create_pid );
        }
    }
    
    say ( 'Granted gPid=' . $gPid );
    
    // organize response to client, including valid token
    $result ["token"] = $t;
    $result ["tgid"] = $tgid;
    $result ["pid"] = $gPid;
    
    // Echo JSON string to mobile
    echo $out = json_encode ( $result );
    
    /**
     * Init Parse.com for push notification
     */
    init_parse_pid ( $gPid, $parse_inst_obj_id );
} else {
    // Error query
    sys_err_stop ();
}
?>
