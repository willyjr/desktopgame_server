<?php
/**
 * CLASS_NAME: init_acc_parse.php
 * PURPOSE:    Upon user registration to aws server, will be called from "r.php" 
 *             then forward user's public_id and Parse's Installation object's ID to
 *             Parse storing in "PublicInfo" table.
 * AUTHOR:     WY
 */
function init_parse_pid($rPid, $rInstObjId) {
    /**
     * Step #1:Read pid and installation id from POST
     */
    $mPid = $rPid;
    $mInstObjId = $rInstObjId; // FIXME:temp code use fake installation id because not used actually, need to fix to actual id before referencing
    
    /**
     * Step 2:Create Relationship in PublicInfo
     */
    $parse_url_create_pubinfo = "https://api.parse.com/1/classes/PublicInfo";
    
    // CURL:init
    $ch = curl_init ();
    
    // Set URL and select POST
    curl_setopt ( $ch, CURLOPT_URL, $parse_url_create_pubinfo );
    curl_setopt ( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
    
    // HEADER
    global $parse_application_id;
    global $parse_master_api_key;
    $post_header_app_id = "X-Parse-Application-Id: " . $parse_application_id;
    $post_header_mstr_k = "X-Parse-Master-Key: " . $parse_master_api_key;
    $post_header_type = "Content-Type: application/json";
    curl_setopt ( $ch, CURLOPT_HEADER, true );
    curl_setopt ( $ch, CURLOPT_HTTPHEADER, array (
            $post_header_app_id,
            $post_header_mstr_k,
            $post_header_type 
    ) );
    
    // DATA
    // TODO:in real life you should use something like:
    // curl_setopt($ch, CURLOPT_POSTFIELDS,
    // $data_where = "";
    // $data_json_to_usr = "";
    // http_build_query(array('postvar1' => 'value1')));
    $post_data = '{
            "pid": "' . $mPid . '",
            "installations":{
                "__type": "Pointer",
                "className": "_Installation",
                "objectId": "' . $mInstObjId . '"
            }
        }';
    say ( 'post data=' . $post_data );
    curl_setopt ( $ch, CURLOPT_POSTFIELDS, $post_data );
    curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
    
    // Send out the query
    $server_output = curl_exec ( $ch );
    $error = curl_errno ( $ch );
    if ($error) {
        sys_err_stop ( 'Create PublicInfo err:' . $error );
    }
    
    // Get objectId of newly created PublicInfo
    $body = getbody ( $server_output, $ch );
    $obj = json_decode ( $body );
    $pubinfo_obj_id = $obj->{'objectId'};
    
    if ($pubinfo_obj_id) {
        say ( 'Succ, pubinfo_obj_id=' . $pubinfo_obj_id );
    } else {
        sys_err_stop ( '[Error][init_acc_parse]error pubinfo_obj_id' );
    }
    
    // CLOSE
    curl_close ( $ch );
    
    /**
     * Step 3:Update key "publicinfo" of Installation to created PublicInfo object
     */
    $parse_url_upd_pubinfo_ptr = "https://api.parse.com/1/installations/" . $mInstObjId;
    
    // CURL:init
    $ch = curl_init ();
    
    // Set URL and select POST
    curl_setopt ( $ch, CURLOPT_URL, $parse_url_upd_pubinfo_ptr );
    curl_setopt ( $ch, CURLOPT_CUSTOMREQUEST, "PUT" );
    
    // HEADER
    curl_setopt ( $ch, CURLOPT_HEADER, true );
    curl_setopt ( $ch, CURLOPT_HTTPHEADER, array (
            $post_header_app_id,
            $post_header_mstr_k,
            $post_header_type 
    ) );
    
    // DATA
    // TODO:in real life you should use something like:
    // curl_setopt($ch, CURLOPT_POSTFIELDS,
    // $data_where = "";
    // $data_json_to_usr = "";
    // http_build_query(array('postvar1' => 'value1')));
    $post_data = '{
            "publicinfo":{
                "__type": "Pointer",
                "className": "PublicInfo",
                "objectId": "' . $pubinfo_obj_id . '"
            }
        }';
    curl_setopt ( $ch, CURLOPT_POSTFIELDS, $post_data );
    say ( 'post data=' . $post_data );
    curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
    
    // Send out the query
    $server_output = curl_exec ( $ch );
    say ( $server_output );
    $error = curl_errno ( $ch );
    if ($error) {
        say ( '[Error] Parse push err:' . $error );
    }
    
    // CLOSE
    curl_close ( $ch );
}
?>
