<?php
/*
 * Push it! send msg to one single target user via Parse.com push service
 */
function push_parse($rcvr_pid, $data) {
    /**
     * Compose POST msg to trigger push via curl
     */
    $ch = curl_init ();
    
    // Set URL and select POST
    global $parse_push_url;
    curl_setopt ( $ch, CURLOPT_URL, $parse_push_url );
    curl_setopt ( $ch, CURLOPT_POST, 1 );
    
    // HEADER
    global $parse_application_id;
    global $parse_rest_api_key;
    $post_header_app_id = "X-Parse-Application-Id: " . $parse_application_id;
    $post_header_rst_k = "X-Parse-REST-API-Key: " . $parse_rest_api_key;
    
    $post_header_type = "Content-Type: application/json";
    
    curl_setopt ( $ch, CURLOPT_HEADER, true );
    curl_setopt ( $ch, CURLOPT_HTTPHEADER, array (
            $post_header_app_id,
            $post_header_rst_k,
            $post_header_type 
    ) );
    
    // DATA
    $data_where = "";
    $data_json_to_usr = "";
    // TODO:in real life you should use something like:
    // curl_setopt($ch, CURLOPT_POSTFIELDS,
    // http_build_query(array('postvar1' => 'value1')));
    global $android_action_key;
    $post_data_query = '{
        "where":{
            "publicinfo": {
                "$inQuery": {
                    "where": {
                        "pid":"' . $rcvr_pid . '"
                    },
                    "className": "PublicInfo"
                }
            }
        },
        "data": {
          "action":"' . $android_action_key . '",
          "msgs":' . $data . '
        }
    }';
    
    curl_setopt ( $ch, CURLOPT_POSTFIELDS, $post_data_query );
    
    // receive server response
    curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
    
    // Send out the query
    $server_output = curl_exec ( $ch );
    $error = curl_errno ( $ch );
    
    // CLOSE
    curl_close ( $ch );
    
    // further processing ....
    say ( 'push_result:' . $server_output );
    if ($error) {
        say ( '[Error] Parse push err:' . $error );
    }
}

function push_gcm($rcvr_reg_id, $data)
{
    /**
     * Compose POST msg to trigger push via curl
     */
    $ch = curl_init ();
    
    // Set URL and select POST
    global $gcm_push_url;
    curl_setopt ( $ch, CURLOPT_URL, $gcm_push_url );
    curl_setopt ( $ch, CURLOPT_POST, 1 );
    
    // HEADER
    global $play_api_key;
    $post_header_api_key = "Authorization: key=" . $play_api_key;
    
    $post_header_type = "Content-Type: application/json";
    
    curl_setopt ( $ch, CURLOPT_HEADER, true );
    curl_setopt ( $ch, CURLOPT_HTTPHEADER, array (
    $post_header_api_key,
    $post_header_type
    ) );
    
    // DATA
    $data_where = "";
    $data_json_to_usr = "";
    // TODO:in real life you should use something like:
    // curl_setopt($ch, CURLOPT_POSTFIELDS,
    // http_build_query(array('postvar1' => 'value1')));
    $post_data_query = '{
        "registration_ids" : ["'. $rcvr_reg_id .'"],
        "data" : {
            "msgs":' . $data . '
        }
    }';
    
    curl_setopt ( $ch, CURLOPT_POSTFIELDS, $post_data_query );
    
    // receive server response
    curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
    
    // Send out the query
    $server_output = curl_exec ( $ch );
    $error = curl_errno ( $ch );
    
    // CLOSE
    curl_close ( $ch );
    
    // further processing ....
    say ( 'push_result:' . $server_output );
    if ($error) {
        say ( '[Error] Parse push err:' . $error );
    }
}
/**
 * TEST CODE
 */

function push_parse_tt($rcvr_pid, $data) {
    /**
     * Compose POST msg to trigger push via curl
     */
    $ch = curl_init ();

    // Set URL and select POST
    global $parse_push_url;
    curl_setopt ( $ch, CURLOPT_URL, $parse_push_url );
    curl_setopt ( $ch, CURLOPT_POST, 1 );

    // HEADER
    global $parse_application_id;
    global $parse_rest_api_key;
    $post_header_app_id = "X-Parse-Application-Id: " . $parse_application_id;
    $post_header_rst_k = "X-Parse-REST-API-Key: " . $parse_rest_api_key;

    $post_header_type = "Content-Type: application/json";

    curl_setopt ( $ch, CURLOPT_HEADER, true );
    curl_setopt ( $ch, CURLOPT_HTTPHEADER, array (
    $post_header_app_id,
    $post_header_rst_k,
    $post_header_type
    ) );

    // DATA
    $data_where = "";
    $data_json_to_usr = "";
    // TODO:in real life you should use something like:
    // curl_setopt($ch, CURLOPT_POSTFIELDS,
    // http_build_query(array('postvar1' => 'value1')));
    global $android_action_key;
    $post_data_query = '{
        "where":{
            "publicinfo": {
                "$inQuery": {
                    "where": {
                        "pid":"' . $rcvr_pid . '"
                    },
                    "className": "PublicInfo",
                    "$limit":"1"
                }
            }
        },
        "data": {
          "action":"' . $android_action_key . '",
          "msgs":' . $data . '
        },
        "$limit":"1"
    }';
    
    curl_setopt ( $ch, CURLOPT_POSTFIELDS, $post_data_query );
    say ( 'post data=' . $post_data_query );

    // receive server response
    curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );

    // Send out the query
    $server_output = curl_exec ( $ch );
    $error = curl_errno ( $ch );

    // CLOSE
    curl_close ( $ch );

    // further processing ....
    say ( 'push_result:' . $server_output );
    if ($error) {
        say ( '[Error] Parse push err:' . $error );
    }
}
?>
