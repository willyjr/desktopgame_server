<?php
// Include
include 'func/general.inc';
include 'func/t.inc';
include 'ccc/sql_acc.inc';
include 'ccc/sql_conn.inc';

// global var
$conn = getSQLConnUser ( $sql_acc, $sql_pwd );

// verify token
$cur_usr = ver_token_get_user_row ( $conn );
// sqlsay('phone=' . $cur_usr->fetch_object()->phone);

// Read update values
$json_string = $_POST ['user'];
$ui = json_decode ( $json_string, TRUE );

// basic check user info before extracting
if (is_null ( $ui )) {
    // TODO:Report error and stop here
    sys_err_stop ();
}

/*
 * Extract user info
 */
$fb_id = $ui ["fb_id"];
$name = $ui ["name"];
$pwd = $ui ["password"];
$gender = $ui ["gender"];
$phone = $ui ["phone"];
$email = $ui ["phone"];
$city = $ui ["city"];
$interests = $ui ["interests"];

/*
 * Routines
 */

/**
 * Start Action here!
 */

sqlsay ( "EOF" );
?>
