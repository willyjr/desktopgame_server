<?php
/**
 * daily refresh ohMe!Q
 * This php will return a newly created ohmeQ for next day watching
 */
header ( "Content-Type:text/html; charset=utf-8" );

// Include
include 'func/general.inc';
include 'ccc/sql_acc.inc';
include 'ccc/sql_conn.inc';

/**
 * Start Action here!
 */

// global var
$conn = getSQLConnUser ( $sql_acc, $sql_pwd );

// verify token and get my uid
$t_row = ver_token_get_token_row ( $conn );
$uid = $t_row->fetch_object ()->uid;
// sqlsay('uid=' . $uid);

$device_id = 1; // FIXME:device should have unique id

/**
 * query if new msg exists
 */

// SQL
// $sql_qry_pending_msg_row = "SELECT * FROM `msg` WHERE `receiver_uid` = '" . $uid . "' AND `mid` > " . $last_mid;
$sql_qry_pending_msg_row = "SELECT * FROM `msg` WHERE `receiver_uid` = '" . $uid . "' AND `fetched` < '" . $device_id . "'";
say ( 'sql_qry_pending_msg_row=' . $sql_qry_pending_msg_row );

// reply
if (($res = $conn->query ( $sql_qry_pending_msg_row ))) {
    // New User
    if ($res->num_rows > 0) {
        // assign count value to output json
        $result ["count"] = $res->num_rows;
        
        $sql_updt_ftchd = "UPDATE `msg` SET `fetched` = '" . $device_id . "' WHERE  `mid` = '0'";
        
        $i = 0;
        while ( $msg_obj = $res->fetch_object () ) {
            $content ['type'] = $msg_obj->ad_type;
            $content ['msg'] = $msg_obj->msg;
            $content ['sender_uid'] = $msg_obj->sender_uid;
            $content ['sender_name'] = $msg_obj->sender_name;
            $content ['timestamp'] = $msg_obj->timestamp;
            $content ['cha_img'] = $msg_obj->cha_img;
            
            $content ['mid'] = $msg_obj->mid;
            $sql_updt_ftchd = $sql_updt_ftchd . " OR `mid` = '" . $content ['mid'] . "'";
            
            $content ['board_icon'] = $msg_obj->board_icon;
            
            $contents_arr [$i] = $content;
            $i ++;
        }
        
        $result ["contents"] = $contents_arr;
        
        echo $out = json_encode ( $result );
        
        // update this msg to fetched
        sqlsay ( "SQL=" . $sql_updt_ftchd );
        $conn->query ( $sql_updt_ftchd );
    } else {
        // TODO:no msg
        say ( 'no msg' );
    }
    
    //
    
    /* free result set */
    $res->close ();
} else {
    // Error query
    sys_err_stop ();
}

?>
