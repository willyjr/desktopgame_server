<?php
/**
 * Return JSON string of undelivered msgs
 */
function getMsgToPush($conn, $receiver_uid) {
    global $android_action_key;
    say ( "WY:key=" . $android_action_key );
    
    // Filter with msgs of same receiver's id and whose fetched_id doesn't contain fetch_id of Parse.com
    global $fetch_id_parse;
    $sql_qry_pending_msg_row = "SELECT * FROM msg T1, users T2 WHERE T1.receiver_uid = '" . $receiver_uid 
            . "' AND (NOT T1.fetched & '"  . $fetch_id_parse . "')" 
            . " AND T1.sender_uid = T2.uid";
    
    say ( 'sql_qry_pending_msg_row=' . $sql_qry_pending_msg_row );
    
    // reply
    if (($res = $conn->query ( $sql_qry_pending_msg_row ))) {
        // New User
        if ($res->num_rows > 0) {
            // assign count value to output json
            $result ["count"] = $res->num_rows;
            
            // enable bit at Parse's fetch id
            $sql_updt_ftchd = "UPDATE `msg` SET `fetched` = `fetched` | '" . $fetch_id_parse . "' WHERE  `mid` = '0'";
            
            $i = 0;
            while ( $msg_obj = $res->fetch_object () ) {
                $content ['type'] = $msg_obj->ad_type;
                $content ['msg'] = $msg_obj->msg;
                $content ['sender_uid'] = $msg_obj->sender_uid;
                $content ['sender_name'] = $msg_obj->sender_name;
                $content ['timestamp'] = $msg_obj->timestamp;
                $content ['timestamp_remind'] = $msg_obj->timestamp_remind;
                $content ['cha_img'] = $msg_obj->cha_img;
                
                $content ['mid'] = $msg_obj->mid;
                $sql_updt_ftchd = $sql_updt_ftchd . " OR `mid` = '" . $content ['mid'] . "'";
                
                $content ['board_icon'] = $msg_obj->board_icon;
                $content ['sender_fb_id'] = $msg_obj->fb_id;
                
                $contents_arr [$i] = $content;
                $i ++;
            }
            
            $result ["contents"] = $contents_arr;
            
            // update this msg to fetched
            sqlsay ( "SQL=" . $sql_updt_ftchd );
            $conn->query ( $sql_updt_ftchd );
            
            $out = json_encode ( $result );
            
            return $out;
        } else {
            // TODO:no msg
            say ( 'no msg' );
        }
        
        /* free result set */
        $res->close ();
    } else {
        // Error query
        sys_err_stop ();
    }
}
?>