<?php
/**
 * Generate a random token of length between min_length~max_length
 */
function getToken($min_length = 32, $max_length = 64) {
    $key = '';
    
    // build range and shuffle range using ASCII table
    for($i = 0; $i <= 255; $i ++) {
        $range [] = chr ( $i );
    }
    
    // shuffle our range 3 times
    for($i = 0; $i <= 3; $i ++) {
        shuffle ( $range );
    }
    
    // loop for random number generation
    for($i = 0; $i < mt_rand ( $min_length, $max_length ); $i ++) {
        $key .= $range [mt_rand ( 0, count ( $range ) )];
    }
    
    $return = base64_encode ( $key );
    
    if (! empty ( $return )) {
        return $return;
    } else {
        return 0;
    }
}
?>