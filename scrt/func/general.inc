<?php
$say = true; // for generic function
$sql_deb_say = true;
$sql_deb_err = true;
$TGID_LEN = 2;
function say($str) {
    global $say;
    if ($say) {
        // Get caller's name from back trace
        $callers = debug_backtrace ();
        $callerName = $callers [1] ['file'];
        if ($callerName === __FILE__) {
            $callerName = $callers [2] ['file'];
        }
        openlog ( $callerName . " say", LOG_PID | LOG_PERROR, LOG_LOCAL0 );
        syslog ( LOG_WARNING, $str );
        closelog ();
    }
}
function sqlsay($str) {
    global $sql_deb_say;
    if ($sql_deb_say) {
        openlog ( "sqlsay", LOG_PID | LOG_PERROR, LOG_LOCAL0 );
        syslog ( LOG_WARNING, $str );
        closelog ();
    }
}
function sqlerr($str) {
    global $sql_deb_err;
    if ($sql_deb_err) {
        openlog ( "sqlerr", LOG_PID | LOG_PERROR, LOG_LOCAL0 );
        syslog ( LOG_WARNING, $str );
        closelog ();
    }
}
/**
 * Report System Internal Error and Stop interpret
 */
function sys_err_stop($str = "") {
    say ( $str );
    http_response_code ( 500 );
    exit ( 0 );
}
function is_token_exist($conn, $token) {
    say ( 'in is_token_exist' );
    
    $result = false;
    $sql_qry_if_t_exists = "SELECT * FROM `tokens` WHERE `token` = '" . $token . "'";
    $res_t = $conn->query ( $sql_qry_if_t_exists );
    sqlsay ( 'token=' . $token . ',rows=' . $res_t->num_rows );
    
    if ($res_t->num_rows > 0) {
        $result = $res_t;
    }
    
    return $result;
}

/**
 * get POST pars and stop if err
 *
 * @param unknown $name            
 */
function get_post_err_stop($name) {
    if (! array_key_exists ( $name, $_POST )) {
        sys_err_stop ( 'missing POST parameter:' . $name );
    }
    
    return $_POST [$name];
}
/**
 * get post pars but don't stop if unavailable
 *
 * @param unknown $name            
 */
function get_post($name) {
    if (array_key_exists ( $name, $_POST )) {
        
        return $_POST [$name];
    }
    return 0;
}

/**
 * verify token and return the row
 *
 * @param unknown $conn            
 * @return unknown
 */
// FIXME:return user by correlating 2 tables:users/tokens
function ver_token_get_user_row($conn) {
    $t = get_post_err_stop ( 'token' );
    if (($res = is_token_exist ( $conn, $t )) == false) {
        sys_err_stop ();
    }
    return $res;
}
function get_uid_by_fb_id($conn, $fb_id) {
    $result = false;
    $sql_qry_if_fbid_exists = "SELECT `uid` FROM `users` WHERE `fb_id` = '" . $fb_id . "'";
    sqlsay ( 'SQL=' . $sql_qry_if_fbid_exists );
    
    $res_t = $conn->query ( $sql_qry_if_fbid_exists );
    
    if ($res_t->num_rows > 0) {
        $uid = $res_t->fetch_object ()->uid;
        return $uid;
    } else {
        sys_err_stop ();
    }
}
function ver_token_get_token_row($conn) {
    $t = get_post_err_stop ( 'token' );
    
    say ( 'in ver_token_get_token_row, token=' . $t );
    
    $result = false;
    $sql_qry_if_t_exists = "SELECT * FROM `tokens` WHERE `token` = '" . $t . "'";
    sqlsay ( 'SQL=' . $sql_qry_if_t_exists );
    
    $res_t = $conn->query ( $sql_qry_if_t_exists );
    
    if ($res_t->num_rows > 0) {
        return $res_t;
    } else {
        sys_err_stop ();
    }
}
function get_token_from_uid($conn, $uid) {
}
function startsWith($str, $prefix) {
    $result = false;
    
    if (strlen ( $str ) > strlen ( $prefix ) && strlen ( $prefix ) > 0) {
        say ( "eq 1. substr=" . substr ( $str, 0, strlen ( $prefix ) ) . ", prefix=" . $prefix );
        if (substr ( $str, 0, strlen ( $prefix ) ) === $prefix) {
            say ( "eq 2" );
            $result = true;
        }
    }
    
    return $result;
}

/**
 * Returns the body content of the specified HTML
 *
 * @param string $html_content
 *            pointer to curl $ch
 * @return string
 */
function getbody($html_content, $ch) {
    $header_size = curl_getinfo ( $ch, CURLINFO_HEADER_SIZE );
    
    $body = substr ( $html_content, $header_size );
    
    say ( 'body=' . $body . ',header_size=' . $header_size );
    
    return $body;
}
function getPidByUid($conn, $uid) {
    $pid = false;
    $sql_qry_get_pid = "SELECT `pid` FROM `pid` WHERE `invalid` = '0' AND `uid` = '" . $uid . "'";
    sqlsay ( 'SQL=' . $sql_qry_get_pid );
    
    $res = $conn->query ( $sql_qry_get_pid );
    
    if ($res->num_rows > 0) {
        $pid = $res->fetch_object ()->pid;
    } else {
        sys_err_stop ( 'Can\' reflect PID from UID:' . $uid );
    }
    
    return $pid;
}

function getPlayRegIdByUid($conn, $uid)
{
    $play_reg_id = false;
    $sql_qry_get_pid = "SELECT `pid` FROM `pid` WHERE `invalid` = '0' AND `uid` = '" . $uid . "'";
    sqlsay ( 'SQL=' . $sql_qry_get_pid );
    
    $res = $conn->query ( $sql_qry_get_pid );
    
    if ($res->num_rows > 0) {
        $play_reg_id = $res->fetch_object ()->play_reg_id;
    } else {
        sys_err_stop ( 'Can\' reflect Play Reg Id from UID:' . $uid );
    }
    
    return $play_reg_id;
}

/**
 * return new msg if exists, otherwise return records
 *
 * @param unknown $conn            
 * @param unknown $token            
 * @return Ambigous <boolean, unknown>
 */
/*
 * function is_new_msg_exist($conn, $token) { $result = false; $sql_qry_if_t_exists = "SELECT * FROM `users` WHERE `token` = '" . $token . "'"; $res_t = $conn->query($sql_qry_if_t_exists); sqlsay('token='.$token.',rows='.$res_t->num_rows); if($res_t->num_rows > 0) { $result = $res_t; } return $result; }
 */
?>
