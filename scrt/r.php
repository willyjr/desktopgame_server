<?php
// Include
include './func/general.inc';
include './func/t.inc';
include './ccc/sql_acc.inc';
include './ccc/sql_conn.inc';
include './pri/init_acc_parse.php';

include './par/parse.inc';
include './par/mstr.inc'; // WY:Make sure need this
                          
// Read from POST
$json_string = $_POST ['user'];
$ui = json_decode ( $json_string, TRUE );

// basic check before extracting
if (is_null ( $ui )) {
    // TODO:Report error and stop here
    sys_err_stop ();
}

/*
 * Extract user info
 */
$fb_id = $ui ["fb_id"];
$name = $ui ["name"];
$pwd = $ui ["password"];
$gender = $ui ["gender"];
$phone = $ui ["phone"];
$email = $ui ["phone"];
$city = $ui ["city"];
$interests = $ui ["interests"];
$parse_inst_obj_id = $ui ["parse_inst_obj_id"];
$play_reg_id = $ui ["play_reg_id"];

/*
 * Routines
 */
function ver_user() {
    global $fb_id;
    // TODO:impl verify user method
    
    return true;
}
function get_sql_insert_user($sqlconn) {
    global $fb_id, $name, $pwd, $gender, $phone, $email, $city, $interests;
    
    $s = "INSERT INTO `users` (`fb_id`, `gender`, `phone`, `email`, `city`, `interests`)" . " VALUES ('" . $fb_id . "','" . $gender . "','" . $phone . "','" . $email . "','" . $city . "','" . $interests . "')";
    
    return $s;
}

/**
 * Start Action here!
 */

// verify user data
if (! ver_user ( $fb_id )) {
    // //TODO:Report error and stop here
    sys_err_stop ();
}

// setup SQL query
$sql_qry_if_fb_exists = "SELECT * FROM `users` WHERE `fb_id` = '" . $fb_id . "'";

// access SQL server
if (! ($conn = getSQLConnUser ( $sql_acc, $sql_pwd ))) {
    // TODO:Report error and stop here
    sqlsay ( 'SQL conn err' );
    sqlerr ( "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error );
}

// query if existed user, insert if yes
if (($res = $conn->query ( $sql_qry_if_fb_exists ))) {
    // New User
    if ($res->num_rows <= 0) {
        // Insert New User to db
        sqlsay ( 'new user' );
        $sql_insert_user = get_sql_insert_user ( $conn );
        sqlsay ( $sql_insert_user );
        $res_i = $conn->query ( $sql_insert_user );
    } else {
        // Existed User
        sqlsay ( 'existed user' );
    }
    
    /**
     * Grant token
     */
    global $TGID_LEN;
    do {
        $t = getToken ();
    } while ( is_token_exist ( $conn, $t ) );
    
    $tgid = substr ( $t, 0, $TGID_LEN );
    
    $sql_get_uid = "SELECT `uid` FROM `users` WHERE `fb_id`='" . $fb_id . "'";
    sqlsay ( $sql_get_uid );
    
    if (($res = $conn->query ( $sql_get_uid ))) {
        $uid = $res->fetch_object ()->uid;
    } else {
        sys_err_stop ();
    }
    
    $sql_exist_t = "SELECT * FROM `tokens` WHERE `uid`= " . $uid;
    // sqlsay($sql_exist_t);
    
    if ($res = $conn->query ( $sql_exist_t )) {
        if ($res->num_rows > 0)         // existed token, update token
        {
            // update token
            $sql_update_t = "UPDATE `tokens` SET  `token` =  '" . $t . "', `tgid`='" . $tgid . "' WHERE  `uid` = " . $uid;
            
            /* free result set */
            $res->close ();
        } else         // token not found, insert to tokens table
        {
            // insert token
            $sql_update_t = "INSERT INTO `tokens` (
							`uid` ,
							`token` ,
							`updated_at` ,
							`tgid`
							)
							VALUES (
							'" . $uid . "',  '" . $t . "',
							" . time () . " ,  '" . $tgid . "'
							);";
        }
    }
    
    // Send UPDATE/INSERT command to SQL server
    $conn->query ( $sql_update_t );
    sqlsay ( $sql_update_t );
    
    /**
     * Grant public ID (pid)
     */
    // Check if pid existed and generate if none
    $sql_exist_pid = "SELECT * FROM `pid` WHERE `invalid` = 0 AND `uid`= " . $uid;
    sqlsay ( $sql_exist_pid );
    
    if ($res = $conn->query ( $sql_exist_pid )) {
        if ($res->num_rows > 0)         // Existed pid, reuse pid
        {
            $gPid = $res->fetch_object ()->pid;
            //TODO:update play_reg_id
        } else {
            // FIXME: New User: Use Parse.com Installation Object ID as user's PID
            // NOTICE:Take care if not using Parse.com service
            $gPid = $parse_inst_obj_id;
            
            // Save pid to db
            $sql_create_pid = "INSERT INTO `pid` (
							`uid` ,
							`pid` ,
                            `play_reg_id`,
							`updated_at`
							)
							VALUES (
							'" . $uid . "',  '" . 
							$gPid . "',  '" . 
							$play_reg_id. "'," .
                            time() . ");";
            sqlsay ( $sql_create_pid );
            $conn->query ( $sql_create_pid );
        }
    }
    
    say ( 'Granted gPid=' . $gPid );
    
    // organize response to client, including valid token
    $result ["token"] = $t;
    $result ["tgid"] = $tgid;
    $result ["pid"] = $gPid;
    
    // Echo JSON string to mobile
    echo $out = json_encode ( $result );
    
    /**
     * Init Parse.com for push notification
     */
    init_parse_pid ( $gPid, $parse_inst_obj_id );
} else {
    // Error query
    sys_err_stop ();
}
?>
