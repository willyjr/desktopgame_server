<?php
/**
 * 
 */
// Include
include 'func/general.inc';
include 'func/msg.inc';
include 'func/t.inc';
include 'ccc/sql_acc.inc';
include 'ccc/sql_conn.inc';
include 'par/parse.inc';

include 'pri/pit.php';

// global var
$conn = getSQLConnUser ( $sql_acc, $sql_pwd );

/**
 * Step #1: Handling incoming msg
 */
// verify token and get my uid
$t_row = ver_token_get_token_row ( $conn );
$sndr_uid = $t_row->fetch_object ()->uid;

// sqlsay('uid=' . $uid);

// Read update values
$receiver_uid = get_post_err_stop ( 'receiver_uid' );

// received a fb_id instead of real uid, need to strip out prefix "fb"
if (startsWith ( $receiver_uid, "fb" )) {
    $fb_id = substr ( $receiver_uid, 2 );
    say ( "startsWith fb, $fb_id=" . $fb_id );
    $receiver_uid = get_uid_by_fb_id ( $conn, $fb_id );
}

$cha_img = get_post_err_stop ( 'cha_img' );
$msg = get_post_err_stop ( 'msg' );
$sndr_nick_name = get_post_err_stop ( 'sender_nick_name' );
$board_icon = get_post_err_stop ( 'board_icon' );
$remind_time = get_post ( 'timestamp_remind' );

$sql_inst_msg_row = "INSERT INTO  `on_hand_media`.`msg` (
`mid` ,
`sender_uid` ,
`receiver_uid` ,
`msg` ,
`ad_type` ,
`fetched` ,
`timestamp` ,
`sender_name` ,
`cha_img`,
`board_icon`,
`timestamp_remind`
)
VALUES (
NULL , '" . $sndr_uid . "', '" . $receiver_uid . "', '" . $msg . "',  'TXT_MSG',  '0',  '" . time () . "',  '" . $sndr_nick_name . "',  '" . $cha_img . "',  '" . $board_icon . "',  '" . $remind_time . "'
);";

say ( 'sql_inst_msg_row=' . $sql_inst_msg_row );

if ($conn->query ( $sql_inst_msg_row )) {
    say ( 'insert msg succ' );
} else {
    // Error query
    sys_err_stop ();
}

/**
 * Step #2: Deliver the msg via push service (Parse.com)
 */
// Collect undelivered msgs of specific receiver_uid
$jsonData = getMsgToPush ( $conn, $receiver_uid );

// Retrieve receiver's Pid by Uid
if (! is_null ( $jsonData ) && strlen ( $jsonData ) > 0 && $gRcvrPid = getPidByUid ( $conn, $receiver_uid )) {
    say ( 'Receiver Pid=' . $gRcvrPid );
    
    push_parse ( $gRcvrPid, $jsonData );
}
?>
