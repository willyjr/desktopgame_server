<?php
/**
 * Update token
 */
// Include
include './func/general.inc';
include './func/t.inc';
include './ccc/sql_acc.inc';
include './ccc/sql_conn.inc';

/*
 * Routines
 */

/**
 * Start Action here!
 */

// global var
$conn = getSQLConnUser ( $sql_acc, $sql_pwd );

// verify token and get my uid
$t_row = ver_token_get_token_row ( $conn );
$uid = $t_row->fetch_object ()->uid;

/**
 * grant token
 */
global $TGID_LEN;
do {
    $t = getToken ();
} while ( is_token_exist ( $conn, $t ) );
$tgid = substr ( $t, 0, $TGID_LEN );

// update token
$sql_update_t = "UPDATE `tokens` SET  `token` =  '" . $t . "', `tgid`='" . $tgid . "' WHERE  `uid` = " . $uid;

// Send UPDATE/INSERT command to SQL server
sqlsay ( $sql_update_t );
$conn->query ( $sql_update_t );

// organize response to client, including valid token
$result ["token"] = $t;
$result ["tgid"] = $tgid;

echo $out = json_encode ( $result );

sqlsay ( "EOF" );
?>