<?php
/**
 * update ohmeQ
 * 
 */

// Include
include 'func/general.inc';
include 'ccc/sql_acc.inc';
include 'ccc/sql_conn.inc';

/**
 * Start Action here!
 */

// global var
$conn = getSQLConnUser ( $sql_acc, $sql_pwd );

// verify token and get my uid
// $t_row = ver_token_get_token_row($conn);//FIXME:Temp skip token check for FG demo(they don't want user login)
// $uid = $t_row->fetch_object()->uid;
// sqlsay('uid=' . $uid);

/**
 * query if new msg exists
 */

// SQL
// Don't check uid as every user share the same ohmeQ
// $sql_qry_get_ohmeq = "SELECT * FROM `ohmeq` INNER JOIN `ad` ON `ohmeq`.`aid` = `ad`.`aid` WHERE `ohmeq`.`uid` =". $uid ." AND `ohmeq`.`valid` = 1;";
$sql_qry_get_ohmeq = "SELECT * FROM `ohmeq` INNER JOIN  `ad` ON `ohmeq`.`aid` = `ad`.`aid` WHERE `ohmeq`.`valid` = 1;";
say ( 'sql_qry_get_ohmeq=' . $sql_qry_get_ohmeq );

// reply
if (($res = $conn->query ( $sql_qry_get_ohmeq ))) {
    // New ohMeQ is ready
    if ($res->num_rows > 0) {
        // assign count value to output json
        $result ["count"] = $res->num_rows;
        
        $i = 0;
        while ( $msg_obj = $res->fetch_object () ) {
            $content ['aid'] = $msg_obj->aid;
            $content ['type'] = $msg_obj->ad_type;
            $content ['media_path'] = $msg_obj->media_path;
            $content ['checksum'] = $msg_obj->checksum;
            $content ['landing_path'] = $msg_obj->landing_path;
            $content ['title'] = $msg_obj->title;
            $content ['subtitle'] = $msg_obj->subtitle;
            $content ['up_ad_aid'] = $msg_obj->up_ad_aid;
            
            $contents_arr [$i] = $content;
            $i ++;
        }
        
        $result ["contents"] = $contents_arr;
        
        echo $out = json_encode ( $result );
    } else {
        // TODO:no msg
        say ( 'no further ohmeQ' );
    }
    
    /* free result set */
    $res->close ();
} else {
    // Error query
    sys_err_stop ();
}

/**
 * Routines
 */

?>
