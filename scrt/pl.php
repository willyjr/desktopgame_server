<?php
// Include
include 'func/general.inc';
include 'ccc/sql_acc.inc';
include 'ccc/sql_conn.inc';

/**
 * Start Action here!
 */

// global var
$conn = getSQLConnUser ( $sql_acc, $sql_pwd );

// No need verify token

/**
 * query if new msg exists
 */
/*
 * $json_string = $_POST['tgid']; //FIXME: tmep code $ui = json_decode($json_string, TRUE); //basic check user info before extracting if(is_null($ui)) { //TODO:Report error and stop here sys_err_stop(); }
 */
// query db
$tgid = get_post_err_stop ( 'tgid' );

// SQL
// FIXME:polling table is not updated correctly, so return always true
$sql_qry_if_polling = "SELECT * FROM `polling`"; // WHERE `tgid` = '" . $tgid . "'";
                                                 // say('sql_qry_if_polling='.$sql_qry_if_polling);
                                                 
// reply
if (($res = $conn->query ( $sql_qry_if_polling ))) {
    $result = array (
            'p' => 0 
    );
    
    if ($res->num_rows > 0) {
        // say('res->fetch_object()->polling_bit='.$res->fetch_object()->polling_bit);
        
        // get polling bit and enable if polling bit if true
        if ($res->fetch_object ()->polling_bit >= 1) {
            $result = array (
                    'p' => 1 
            );
        }
    }
    
    echo $out = json_encode ( $result );
    
    /* free result set */
    $res->close ();
} else {
    // Error query
    sys_err_stop ();
}

/**
 * Routines
 */

?>
